### Imágenes live personalizadas arrancadas por PXE.
### Ricardo Marcelo Rando
### isx46360391

# ¿Qué es una distribución de tipo Live?

Una distribución live, más genéricamente &ldquo;Live Distro&rdquo;, es un sistema 
operativo almacenado en un medio extraíble, tradicionalmente un CD o un 
DVD que puede ejecutarse directamente en una computadora.

Normalmente, un Live viene acompañado de aplicaciones e incluyen una 
herramienta que permite instalarlos en el disco duro.

Otra característica es que por lo general no se efectúan cambios en 
el ordenador utilizado. No se requiere instalación, por lo que no hay 
que tocar el disco duro. Los datos, particiones o sistemas operativos 
del disco duro no se pierden

Uno de los mayores inconvenientes de este sistema es el requerimiento
de una gran cantidad de memoria RAM, una parte para su uso habitual y
otra para funcionar como el disco virtual del sistema.

En el caso de Linux, encontramos el Proyecto Syslinux. Este posee
cargadores de arranque para disquetes (**Syslinux**), arranque desde la red
(**Pxelinux**), y CD-ROM arrancables bajo la denominación &ldquo;El Torito&rdquo;,
también llamados Live CD (**Isolinux**).

# Tipos de distribuciones Live

- **Desktop** (escritorio): Diseñada especialmente para aquellos usuarios
que quieran &ldquo;testear&rdquo; un sistema operativo sin tener que comprometerse
con él, ya que no requiere instalación, pero dispone de casi todas las 
aplicaciones y/o herramientas del producto final. Quizás sea la 
distribución live más utilizada. 

- **Instalador**: Distribución que cómo su nombre bien indica, es utilizada
para instalar un sistema operativo. Normalmente una vez iniciada,
el usuario va eligiendo y siguiendo los pasos. También disponemos de
versiones desatendidas, en las que solo deberemos esperar a que la
instalación lo haga todo de forma  automática.

- **Rescue**: Son distribuciones especializadas en recuperar sistemas
operativos dañados.

- **Tools**: Herramientas y útiles que se utilizan habitualmente en la 
fase previa a que un sistema arranque, ya sea para comprobar el estado 
de la memoria (memtest), &ldquo;trastear&rdquo; la particiones (GParted) u otras 
cosas similares.

- **Kiosko**: Distro en la que solo se tiene acceso a aplicaciones
determinadas y todo lo demás queda bloqueado. Normalmente se usan con
un software específico de una empresa, por ejemplo un cajero automático.
También es muy habitual encontrarnos este tipo de distribuciones en
bibliotecas, locutorios, etc. En estos últimos, tenemos un escritorio 
de uso muy limitado y generalmente con un navegador de internet.

# Como crear tu propia distribución

Para crear una imagen live, necesitaremos la herramienta llamada
**livecd-creator**. (Para poder utilizar dicha utilidad necesitaremos
poseer derechos de superusuario). Para ello nos logearemos como usuario
**root** y nos descargaremos el paquete **livecd-tools**, ya que esta se 
encuentra entre una de sus utilidades.

	$ sudo yum install livecd-tools

El siguiente paso a realizar sería crear el archivo de configuración de 
la imagen live que queremos crear. A este se le conoce como fichero 
**kickstart**. En realidad, un fichero en formato kickstart, con 
extensión `.ks`, no es más que un fichero que determina los pasos que 
realizará Anaconda (software de instalación) en una instalación 
automatizada. Los archivos de configuración se componen de varias 
partes. En la primera se describe la configuración básica de los 
elementos del sistema tales como el idioma, teclado, región horaria, 
etc.. En esta parte también podemos incluir otras configuraciones 
creadas previamente. (`% include config.ks`). Después escogeríamos los 
paquetes que deseamos que se instalen y por último podemos redactar un 
script que será ejecutado al final del proceso de construcción.

Si no se es un usuario avanzado en estas técnicas, es aconsejable
basarse en configuraciones pre-hechas o hechas por otros usuarios. A
parte podremos encontrar scripts de *post-instalación* útiles y
muy interesantes. Podemos encontrar muchos *spin-kickstarts* (ejemplos) 
descargando el paquete **'spin-kickstarts'**. Una vez instalado, los
podremos encontrar en `/usr/share/spin-kickstarts/` 

Existe una aplicación llamada `system-config-kickstart` que nos permite
crear ficheros de configuración a nuestro antojo. Esta herramienta fue
creada para ficheros kickstart de instalaciones automatizadas, pero nos
puede ser muy útil en la creación de las nuestras. 

Más adelante podemos ver un ejemplo bastante sencillo.

	# Arxiu de configuració centos personalitzat
	# idioma
	lang en_US.UTF-8
	# idioma teclat
	keyboard es
	# zona horaria
	timezone Europe/Andorra
	# informacio autoritzacio del sistema
	auth --useshadow  --passalgo=sha512
	# configuracio SELinux
	selinux --disabled
	# configuracio firewall
	firewall --disabled
	xconfig --startxonboot
	part / --size 3072 --fstype ext4
	services --enabled=NetworkManager --disabled=network,sshd
	# Root password
	rootpw --iscrypted $1$Pctk5lBf$MGCO0I/b22UZfszuINBnc/
	repo --name=fedora --baseurl=file:///var/tmp/isocentos
	# paquets que volem afegir
	%packages
	@X Window System
	@GNOME Desktop Environment
	@guest-desktop-agents
	@core
	@fonts
	@multimedia
	@firefox
	# xorg-x11-init
	# Explicitly specified here:
	# This was added a while ago, I think it falls into the category of
	# "Diagnosis/recovery tool useful from a Live OS image".  Leaving this untouched for now.
	memtest86+
	# The point of a live image is to install
	@anaconda-tools
	# Make live images easy to shutdown and the like in libvirt
	qemu-guest-agent
	%end
	# Script post instalació
	# %post
	# %end


Para crear la imagen, solo debermos seguir el siguiente comando:

	$ sudo livecd-creator --verbose --config=/path/to/kickstart/file.ks --fslabel=Image-Label

Después de este paso, ya tendremos nuestra imagen lista para disfrutarla en el soporte deseado.

# Soportes para distribuciones live

El soporte por el cual se ofrezca una imagen live, deberá estar 
admitido por la bios del ordenador que quiera disfrutar de ella. 

## CD o DVD

Su uso siempre fue el más popular hasta la llegada de los tan 
conocidos, como usados pendrives. La utilización de distribuciones live 
es muy sencillo. Solo necesitamos tener la imagen en cuestión y 
quemarla con una aplicación sobre el disco. En nuestro caso 
utilizaremos brasero que es la que viene por defecto instalada en 
escritorios Gnome. Simplemente seguimos estos pasos:

1. Abrir la aplicación Brasero.
2. Hacer click en grabar una imagen.
3. Clickar sobre seleccionar una imagen y vamos navegando hasta encontrar la deseada.
4. Insertar un disco en blanco, y luego le damos botón de crear imagen.

Existe la posibilidad de hacerlo mediante la línea de comandos. Pero es 
bastante más fácil con esta aplicación y que además ya está instalada. 

## Dispositivo USB

Las tendencias cambian, la tecnología avanza y en este caso no iba a 
ser menos. Actualmente, nadie usa ya, un dispositivo de bloque en 
formato CD/DVD. La mayoría de ordenadores y dispositivos portátiles que 
se venden hoy en día, ya no disponen de lector de estos. La tecnología 
USB hace ya mucho que revolucionó el mercado, y ¿quien no tiene un 
&ldquo;cacharrito&rdquo; de estos?. Por lo tanto, en lo que a nosotros 
nos concierne, también nos vemos obligados a explicar como disfrutar de 
nuestras distribuciones live mediante un pendrive.

El primer paso será descargar el programa LiveUSB Creator.

	$ sudo yum install liveusb-creator
	
Luego procederemos insertando el stick y ejecutando el programa. Para 
usar la herramienta, simplemente seleccionamos una versión de Fedora 
para descargar desde la lista desplegable en la parte superior derecha 
o seleccionamos una ISO que tengamos en el sistema de ficheros, 
pulsando en  el botón `Examinar` en la parte superior izquierda. 

Después seleccionamos el dispositivo USB en el que se desea escribir la 
imagen, en el apartado `Dispositivo destino` que se encontrara en una lista 
desplegable, y pulsamos el botón `Crear USB Live`.  

En esta ocasión se ha utilizado esta herramienta. Hay varias maneras de 
conseguir el mismo resultado, pero creemos que esta era una rápida y 
sencilla.

## PXE

Preboot eXecution Environment (PXE) (Entorno de ejecución de 
pre-arranque), es un entorno para arrancar e instalar el sistema 
operativo en ordenadores a través de una red, de manera independiente 
de los dispositivos de almacenamiento de datos disponibles o de los 
sistemas operativos instalados.

En nuestro caso, no queremos instalar ninguna distribución, la 
intención de este proyecto es poder arrancar imágenes tipo live vía 
PXE, y ahora detallaremos paso a paso, que es lo que se necesita para 
llegar a conseguirlo, sin morir en el intento.

Deberemos tener al menos tres servicios en marcha para poder ofrecer 
PXE. DHCP y TFTP son siempre obligatorios. Luego el tercero dependerá 
de los gustos y/o necesidades del servidor. Podrían ser por ejemplo FTP 
o HTTP, nosotros nos decantamos por HTTP, y por eso será el que 
expliquemos. Si se quisiese utilizar otros solo habría que aplicar los 
pasos pero con ese servicio en concreto.

Realmente no existe un orden para la configuración de los servicios, 
pero bajo nuestro criterio, creemos que primero, el servidor debería 
ser capaz de poder ofrecer archivos vía http y tftp, ya que estos serán 
un pilar fundamental en nuestro proceso. Después de estos, deberíamos 
ser capaces de poder ofrecer direcciones IP con los archivos vinculados 
a estas.

### HTTP

Para empezar, deberemos tener instalado el paquete necesario.

	$ sudo yum install httpd

Arrancamos el servicio.

	$ sudo systemctl start httpd.service

Ahora comprobaremos que el servicio puede proporcionar archivos. Para 
probar solo necesitamos introducir cualquier archivo en `/var/www/html` 
que es el directorio de publicación por defecto y ejecutuar el comando 
`telnet` .

	$ telnet 192.168.0.10 80
	Trying 192.168.2.10...
	Connected to 192.168.0.10.
	Escape character is '^]'.
	GET /prueba
	¡Esto es una prueba de que el server http 
	funciona a las mil maravillas!
	Connection closed by foreign host.

¿Simple verdad? Bien, ya tenemos un servidor que nos proporciona 
archivos de forma remota. Ahora seguimos con TFTP.

### TFTP

En un principio tftp era un servicio controlado por el superservidor 
**xinetd**, per actualmente funciona como un servicio stand-alone. Por 
lo tanto procederemos a descargarlo y arrancarlo como cualquier otro 
servicio.

Paquetes de instalación.

	$ sudo yum tftp-server

Arrancamos el servicio.

	$ sudo systemctl start tftp.socket
	$ sudo systemctl start tftp.server

Para comprobar que nuestro servidor tftp nos brindará los archivos que 
requiramos de él, introduciremos un archivo de prueba en el directorio 
`/var/lib/tftpboot/`, que es el directorio de publicacón por defecto y 
ejecutaremos el comando `tftp`.

	tftp 192.168.0.10
	tftp> verbose
	Verbose mode on.
	tftp> get prueba
	getting from 192.168.0.10:prueba to prueba [netascii]
	Received 419 bytes in 0.0 seconds [922648 bit/s]

De momento nada complicado. Sigamos con la explicación.

### DHCP

Pasaremos a configurar nuestro servidor para que sea el que administre 
la red y proporcione gracias a TFTP lo archivos que queramos ofrecer.

Instalación de paquetes necesarios.

	$ sudo yum dhcp

Al instalar este paquete, se crea el fichero  `/etc/dhcp/dhcp.conf`, 
que es el de configuración del servidor. Este estará vacío y nosotros 
pasaremos a rellenarlo manualmente. Debería quedarnos algo similar 
a esto.

	#dhcpd.conf
	subnet 192.168.0.0 netmask 255.255.255.0 {
	option routers 192.168.0.10;
	range 192.168.0.11 192.168.0.50;
	next-server 192.168.0.10;
	filename "pxelinux.0";
	}


Ahora pasaremos a configurar nuestra tarjeta de red para que tenga una 
dirección estática. 

	$ sudo /etc/sysconfig/network-scripts/ifcfg-eth0

Aplicaremos los cambios necesarios para que nos quede una configuración 
parecida a esta y podremos proceder a encender el servicio y probar que 
asigna correctamente las ips.

	# Generated by dracut initrd
	# Archivo de configuración tarjeta de red eth0
	DEVICE="eth0
	ONBOOT=yes
	BOOTPROTO=none
	IPADDR=192.168.0.10
	PREFIX=24

En lo que a servicios se refiere, deberíamos tener preparado nuestro 
servidor, para poder ofrecer cualquier tipo de distribución por PXE.

### Configurando PXE

Para arrancar una imagen, debemos ofrecer al cliente el núcleo del 
sistema operativo (**kernel**) y un sistema de archivos temporal usado 
por este, para que pueda hacer los arreglos necesarios antes de montar 
la raíz (**initrd**). Actualmente no bastaría solo con estos dos, ya 
que las imágenes son de gran tamaño y dejarían a las computadoras sin 
memoria, por eso necesitamos que parte de la imagen sea ofrecida por el 
servidor, por otras vías tales como http. Estas normalmente vienen 
comprimidas en formato `squashfs`. 

Podemos conseguir tanto el kernel como las imágenes init en las páginas 
oficiales de los sistemas operativos tales como Fedora o Centos. Se 
pueden descargar directamente buscando un poco. En principio descargando
las isos de las versiones net-install, montándolas y navegando por ellas,
también podemos encontrarlas. 

Las imágenes comprimidas del sistema de archivos, las podemos encontrar 
montando una iso live y extrayendo los archivos que se encuentran el 
carpeta que siempre se llama `LiveOS`.
 
Vamos a pasar a configurar lo que vendría siendo PXE en esencia.

Descargaremos el paquete syslinux, ya que en él se encuentran los 
archivos necesarios para la correcta configuración de nuestro servidor 
PXE y los copiaremos en la carpeta `/var/lib/tftpboot`.

Syslinux es una colección de cargadores de arranque capaz de iniciar 
desde discos duros, CDs, y vía network utilizando PXE. 

	$ sudo yum install syslinux

Una vez descargado el paquete, pasaremos a crear la estructura y a 
copiar los archivos de configuración necesarios. Más adelante iremos 
viendo para que sirven cada uno de ellos.

	$ sudo cp /usr/share/syslinux/pxelinux.0 /var/lib/tftpboot/
	$ sudo cp /usr/share/syslinux/vesamenu.c32 /var/lib/tftpboot/
	$ sudo mkdir /var/lib/tftpboot/pxelinux.cfg
	$ sudo mkdir /var/lib/tftpboot/live
	$ sudo mkdir /var/www/html/live

Vayamos por pasos.

El archivo pxelinux.0 es el que ofreceremos con el servidor dhcp, vía 
tftp y este será el encargado de vincularnos con pxe. 

Vesamenu.c32 es el archivo de menú de arranque. 

En la carpeta pxelinux.cfg es donde se encontrarán los archivos de 
configuración de los menús pxe (por defecto será default).

La carpeta live, (en este caso se llama así), es donde pondremos 
nuestro kernel e imagen initrd para que puedan ser arrancadas por el 
cliente en el primer caso, y en el segundo será donde se encuentre la 
imagen `squashfs` ofrecida vía http.

Con estos pasos ya tenemos todos los elementos necesarios de la 
estructura, ahora solo nos falta configurar el archivo del menú y ya 
tendremos nuestro servidor pxe listo para la acción. 

Nos debería quedar algo semejante a la siguiente figura.

	tftpboot						/var/www/html
	├── live							└──── live
	│   ├── initrd.img					   └── squashfs.img
	│   └── vmlinuz					
	├── pxelinux.0					
	├── pxelinux.cfg				
	│   ├── default					
	└── vesamenu.c32				

En el siguiente ejemplo que encontramos en 
`/var/lib/tftpboot/pxelinux.cfg/default` podremos ver la configuración 
básica del archivo de menú pxe, el cual contiene sus opciones también.

	default live
	prompt 0
	timeout 300
	
	MENU TITLE PXE Menu

	LABEL live
		KERNEL live/vmlinuz
		 APPEND initrd=live/initrd.img root=live:http:/xxx.xxx.xxx.xxx/live/squashfs.img

Solo necesitamos conectar él o los ordenadores que queramos arrancar por
pxe y a disfrutar.

### imágenes y menús PXE

Una vez hemos conseguido tener un servidor pxe básico, avanzaremos un 
poco más añadiendo imágenes de fondo y configurando a distintos niveles
nuestro/s menú/s.

Para crear un menú multinivel, empezaremos editando un archivo que será 
el que haga de menú principal el cual derivaremos hacía los otros que 
harán de submenús.

Examinemos los siguientes ejemplos.

	#archivo del menú principal
	DEFAULT vesamenu.c32
	PROMPT 0
	MENU BACKGROUND images/index.jpg
	MENU TITLE PXE MENU

	LABEL ^1 Instaladores
			KERNEL vesamenu.c32
			APPEND pxelinux.cfg/instaladores
			TEXT HELP
			Instaladores de sistemas operativos
			ENDTEXT

	LABEL ^2 Versiones Live
			KERNEL vesamenu.c32
			APPEND pxelinux.cfg/live
			TEXT HELP
			Versiones de tipo Live
			ENDTEXT

	LABEL ^3 Herramientas
			KERNEL vesamenu.c32
			APPEND pxelinux.cfg/herramientas
			TEXT HELP
			herramientas para particionar,rescatar y utilidades para sistemas operativos
			ENDTEXT

Este es el archivo `/var/lib/tftpboot/pxelinux.cfg/default` el cual 
hace de menú principal, nos muestra todos los submenús disponibles y nos
redirige hacia ellos.

El siguiente ejemplo es un submenú con varias opciones. Explicaremos 
cuales son y como usarlas.

	#submenú de distribuciones live
	DEFAULT vesamenu.c32
	PROMPT 0
	TIMEOUT 0

	MENU BACKGROUND images/splash.jpg
	MENU TITLE Live

	LABEL ^1 Centos Live 7 Desktop
			KERNEL centos7/vmlinuz
			APPEND initrd=centos7/initrd.img root=live:http://192.168.0.10/centos7live/squashfs.img
			TEXT HELP
			Live de centos 7 desktop
			ENDTEXT

	LABEL ^2 Tiny Core Live
			KERNEL tiny/vmlinuz
			APPEND initrd=tiny/initrd.img boot=live
			TEXT HELP
			Mini Linux Live
			ENDTEXT

	LABEL ^3 Fedora Live KDE 21
			KERNEL fedora21/vmlinuz
			APPEND initrd=fedora21/initrd.img root=live:http://192.168.0.10/fedora-live-kde-21/squashfs.img

	LABEL ^4 Fedora Live Workstation 21
			KERNEL fedora21/vmlinuz
			APPEND initrd=fedora21/initrd.img root=live:http://192.168.0.10/fedora-live-workstation-21/squashfs.img

	LABEL ^5 Fedora Live LXDE 20
			KERNEL fedora20/vmlinuz
			APPEND initrd=fedora20/initrd.img root=live:http://192.168.0.10/fedora-live-lxde-20/squashfs.img

DEFAULT: Opción de arranque por defecto. 

PROMPT: Si su valor es 0, no muestra el boot prompt, en cambio si es 1 lo muestra. 

TIMEOUT: Indica cuanto tiempo tardará en seleccionar la línea puesta por defecto. 

F[1-12] filename: Muestra el archivo indicado por pantalla al pulsar el botón indicado.
Normalmente es usado para menús de ayuda.

MENU BACKGROUND: se le puede pasar una imagen .jpg o decidir que color quieres para el fondo de tu menú.

MENU TITLE: El título que se mostrará en nuestro menú.

MENU RESOLUTION: Define la resolución del fondo del menú. Por defecto es 640x480

MENU PASSWD: Solicitar el password que se seleccione.

LABEL: Nombre que se mostrarán de las opciones.

KERNEL: El kernel que queremos arrancar.

APPEND: Opciones con las que queremos que nuestro kernel arranque. Es 
el equivalente al append del sistema de arranque `LILO`. 

Las opciones que se le pueden pasar a append son:

	Live Distro Boot Parameters

	all_generic_ide
	Options/Value: none, standalone flag
	Description: Load the ata_generic driver during boot up for generic IDE support.
	Example: all_generic_ide

	blacklist=
	Options/Value: unwanted-module
	Description: Specify a comma separated list of unwanted modules or device drivers.
	The hardware detection algorithms in initramfs may sometimes select a module that 
	does not perform well on your system. Occasionally, you can blacklist that module 
	to obtain different or possibly better results from a more generic fallback module.
	Example: blacklist=nouveau
	Example: blacklist=radeon

	boot=
	Options/Value: live, local, nfs
	Description: Specify the type of root filesystem that should be mounted during the
	boot process. Keep in mind that if you select "boot=local" or "boot=nfs" then the 
	live boot related parameters in the list are ignored. Such live boot parameters include
	those related to persistence, live media search, unionfs, COW, toram, etc. The default
	seems to be local, so you should always specify "boot=live" when booting the TAILS distro
	from secure, readonly media.
	Example: boot=live

	BOOTIF=
	Options/Value: <pxe-formatted-mac-address>
	Description: pxelinux uses this setting to tell the initramfs which mac address belongs to 
	the NIC over which the pxe boot occurred. You should probably *not* attempt to specify this 
	option with your kernel parameters. Instead, you should use either "ethdevice=" or "ip=" to 
	specify specific network devices that you wish to be setup by initramfs during the boot process.
	Example: BOOTIF=01-66-55-44-33-22-11

	break=
	Options/Value: top, modules, premount, mount, mountroot, bottom, init
	Description: Interrupt the initramfs boot script at the specified stage. This causes the 
	script processing to halt and drops the console user to a shell. This parameter is primarily 
	used for debugging the boot process. It allows you to examine the filesystem and module status
	within the initramfs environment. The default is to keep going, with no interruption. If you
	specify "break" with no value (no equals), then it will stop at the premount stage.
	Example: break=modules

	cryptopts=
	Options/Value: target, source
	Description: Specify options needed to support mounting the root filesystem with encryption enabled.
	Example: cryptopts=

	debug or debug=
	Options/Value: anything
	Description: Put the boot script in debug mode. This enables the output of each command to 
	the console as it is executed via the "set -x" shell option. Automatically disables quiet mode.
	If "debug" is used as a standalone flag (no equals), then output is redirected to 
	"/run/initramfs/initramfs.debug". If debug is used with a value (debug=something), then output 
	of each command is still enabled, but there is no redirection. In this case the "something" or value
	may be interpreted by the kernel, but the value is ignored by the initramfs boot script.
	Example: debug
	Example: debug=something

	dhcp
	Options/Value: none, standalone flag
	Description: Force the use of DHCP for networking configuration. This can be used to force DHCP when
	it would normally be disabled, such as when using "netboot=". The default is to use DHCP. To disable
	DHCP, see the "nodhcp" flag.
	Example: dhcp

	ethdevice=
	Options/Value: eth0,eth1,...
	Description: A comma separated list of network interface device labels on which to setup networking
	 for the live system. This can be useful when there are multiple NICs present in the system and you
	  wish to boot or configure for a specific network interface.
	Example: ethdevice=eth0,eth1

	ethdevice-timeout=
	Options/Value: [interface-config-timeout]
	Description: Specify the timeout allowed while configuring the network interfaces. The default
	network configuration timeout is 15 seconds.
	Example: ethdevice-timeout=25

	exposedroot
	Options/Value: none, standalone flag
	Description: Allow the root directory of the live media to be fully exposed (writable) to the
	running live system. Normally, the merged filesystem (via aufs/unionfs) prevents updates to the
	live media, even if it is a writable block device. With this flag, file system updates outside
	of the mounted persistence areas will be written back to the live media partition. Use this
	flag at your own risk.
	Example: exposedroot

	fetch=
	Options/Value: <root-fs-url>
	Description: Specify a URL for obtaining the root filesystem image. This boot option allows you 
	to download the root filesystem for the live system over the network. This would be most useful 
	in a PXE or thin client environment. Supported file types/extensions for the filesystem image 
	include iso, squashfs, tgz and tar. This option is different from "ftpfs=" and "httpfs=" in that
	it downloads the entire filesystem image before mounting it. The benefit of the fetch approach 
	is that access to the filesystem is much faster after the download. The drawback is that it can
	consume lots of memory.
	Example: fetch=tftp://192.168.2.3/tftproot/live/rootfs.tgz
	Example: fetch=https://bootserver.internal/liveboot/debian-live-rootfs.tgz

	findiso=
	Options/Value: <iso-file-with-root-fs>
	Description: Directs the initramfs init script to look for the given ISO path/file on all of the
	devices/partitions where it would normally search for a squashfs root filesystem image. This 
	allows the live system to run from the root filesystem inside the squashfs image that is contained
	within the ISO image This is primarily useful when multibooting various distros from the same USB
	device and you do not wish to extract the individual ISOs into separate partitions.
	Example: findiso=/live/mydistro.iso

	fromiso=
	Options/Value: <iso-file-with-root-fs>
	Description: Directs the initramfs init script to look for the given ISO path/file on the specified
	block devices/partition. This allows the live system to run from the root filesystem inside the
	squashfs image that is contained within the ISO image This is primarily useful when multibooting
	various distros from the same USB device and you do not wish to extract the individual ISOs into
	separate partitions. The "findiso=" option performs a broader search, however, this option allows
	you to specify the exact location for the ISO file. Given the unpredictability of drive numbering 
	in the many BIOS implementations, you are strongly encouraged to refer to the block device by the
	UUID. Same as "isofrom=".
	Example: fromiso=/dev/sda1/live/mydistro.iso
	Example: fromiso=/dev/disk/by-uuid/c02dcbff-3222-4555-b333-c2351b73f88b/live/mydistro.iso

	ftpfs=
	Options/Value: <root-fs-ftp-url>
	Description: Specify an FTP URL for obtaining the root filesystem image. This boot option allows
	you to mount the root filesystem for the live system via curlftpfs and the fuse module over the 
	network. This would be most useful in a PXE or thin client environment. Supported file 
	types/extensions for the filesystem image include iso and squashfs. This option is different
	from "fetch=" in that it mounts the filesystem image over the network. The benefit of this
	approach is that it does not need to download the entire image first. The drawback is that 
	the network must remain accessible and performance may suffer when accessing the server for new files.
	Example: ftpfs=ftp://192.168.2.3/liveboot/rootfs.squashfs

	httpfs=
	Options/Value: <root-fs-http-url>
	Description: Specify an HTTP URL for obtaining the root filesystem image. This boot option allows
	you to mount the root filesystem for the live system via httpfs and the fuse module over the network.
	This would be most useful in a PXE or thin client environment. Supported file types/extensions for 
	the filesystem image include iso and squashfs. This option is different from "fetch=" in that it mounts
	the filesystem image over the network. The benefit of this approach is that it does not need to download
	the entire image first. The drawback is that the network must remain accessible and performance may
	suffer when accessing the server for new files.
	Example: httpfs=http://192.168.2.3/liveboot/rootfs.squashfs

	ignore_uuid
	Options/Value: none, standalone flag
	Description: Disable the UUID verification against the file(s) in "/.disk/live-uuid*". The default is 
	for the initramfs boot script to look for a matching UUID marker and ignore any partition that does not
	have a matching UUID.
	Example: ignore_uuid
	
	initrd=
	Options/Value: <image-file>
	Description: Specify ramdisk initializer
	Example: initrd=initrd.img
	
	ip=
	Options/Value: frommedia, dhcp, bootp, rarp, both, on, any, none, off
	Description: Specify a source for the IP address and network configuration or disable networking. 
	If you specify "dhcp" then the initramfs boot script with attempt to look up the IP address and network
	configuration using DHCP on the local network. The "bootp" and "rarp" options are similar to "dhcp" but
	use the older protocols. Apparently, "on" and "any" are synonyms for trying all three: dhcp, bootp and
	rarp. The "none" and "off" options will disable network setup, outside of possible PXE options. With
	"frommedia", you must provide the network configuration via /etc/network/interfaces on the live media 
	filesystem. The default is to use DHCP. If DHCP is disabled and "ip=" is not specified, then the boot
	script will assume "frommedia" and look for /etc/network/interfaces.
	Example: ip=dhcp
	Example: ip=frommedia
	Example: ip=off

	ip=
	Options/Value: <[device]:[static-ip]:[netmask]:[gateway]:[nameserver]>
	Description: Specify a fixed or static IP address and network configuration. This is necessary when
	DHCP is disabled, such as with "netboot=" or "nodhcp". You should specify the configuration as a colon
	separated list of values or you can give specify a source such as "frommedia". The default is to use
	DHCP, in which case "ip=" is ignored. If DHCP is disabled and "ip=" is not specified, then the boot
	script will assume "frommedia" and look for /etc/network/interfaces.
	Example: ip=eth0:192.168.1.42:192.168.1.0:192.168.1.1:192.168.1.1

	isofrom=
	Options/Value: <iso-file-with-root-fs>
	Description: Directs the initramfs init script to look for the given ISO path/file on
	the specified block devices/partition. See "fromiso=" for more details.
	Example: isofrom=/dev/sda1/live/mydistro.iso
	Example: isofrom=/dev/disk/by-uuid/c02dcbff-3222-4555-b333-c2351b73f88b/live/mydistro.iso

	live-config
	Options/Value: none, components, nocomponents
	Description: Only run with boot=live. It contains the components necessaries for run live system.
	(late userspace)
	Example: live-config.components 

	live-media=
	Options/Value: removable-usb, removable, <live-fs-device>
	Description: Specify the location of the live distro root filesystem. Same as "bootfrom=".
	Example: live-media=removable-usb

	live-media-offset=
	Options/Value: Any positive integer. Defaults to zero.
	Description: The offset in bytes to the beginning of data in the static (read-only) root 
	filesystem image inside the ISO.
	Example: live-media-offset=0

	live-media-path=
	Options/Value: live
	Description: Directory path to find the static (read-only) root filesystem image inside the ISO.
	Do not include the filename of the root filesystem image, only the directory path. The root
	filesystem image should have an extension and type of *.squashfs, *.ext2, *.ext3, *.ext4,
	*.jffs2 or *.dir.
	Example: live-media-path=live

	live-media-timeout=
	Options/Value: Any integer from 0 to 59. Defaults to zero.
	Description: The length of time in seconds to wait before searching for the live media root
	filesystem. If you specify a number larger than 59 seconds, the script will always fail to 
	find the live media root filesystem. Some local devices, such as USB or CDROM drives may need
	extra time to become available to the kernel.
	Example: live-media-timeout=10

	module=
	Options/Value: Filename without the ".module" extension.
	Description: Rename the optional, default "filesystem.module" list of filenames for disk images
	to be merged into the aufs stack for the root filesystem. The file specified must be located
	in the "/live" directory.
	Example: module=mylist

	netboot=
	Options/Value: nfs, cifs, iscsi
	Description: Request that the live system and kernel boot up with a networked root file system. 
	This option normally disables DHCP network configuration, so you should specify the static network 
	configuration with "ip=". When selecting nfs, be sure to also specify nfsroot=, nfsopts= and 
	nfsoverlay= as needed. When selecting cifs, be sure to also specify "nfsroot=" and "nfsopts=", 
	since those are used with cifs netboot mounts. When selecting iscsi, be sure to also specify 
	iscsi= as needed.
	Example: netboot=nfs

	netconsole=
	Options/Value: <port>@<ip-address>/<network-device>
	Description: Enable the use of kernel logging over the network via the netconsole module.
	Use the nc (or netcat) command to establish a listener on a separate system to receive the
	netconsole output. The default is to not send kernel logging over the network.
	Example: netconsole=4444@192.168.1.42/eth0
	Example: netconsole=3333@192.168.1.42/07:e3:32:22:f0:a5

	nfsopts=
	Options/Value:
	Description: Custom options for the NFS mount of the root filesystem when netboot=nfs.
	Example: nfsopts=

	nfsoverlay=
	Options/Value: <nfs-mountpoint>
	Description: The NFS mount area for the copy-on-write storage used by the live system. 
	This option provides a way for each system to have a separate area on the NFS server for 
	any files that are created or updated from the underlying (read-only) root filesystem. 
	This is known as the COW (copy on write) part of the aufs filesystem stack. 
	This setting only applies when netboot=nfs.
	Example: nfsoverlay=192.168.44.55:/nfs/workstation7

	nfsroot=
	Options/Value: nfshost:mountpath
	Description: The hostname and mount path for the nfs mount of the root filesystem.
	Example: nfsroot=192.168.44.55:/nfs/sharedroot

	nodhcp
	Options/Value: none, standalone flag
	Description: Disable the use of DHCP for networking configuration. This flag can be used
	to prevent DHCP when it is unavailable or undesired. If you choose to disable DHCP, 
	the you should specify the networking configuration explicitly ("ip=") or disable networking
	("nonetworking"). To enable DHCP, see the "dhcp" flag.
	Example: nodhcp

	nofstab
	Options/Value: none, standalone flag
	Description: Do not update the /etc/fstab to contain the unionfs nor tmpfs filesystems created
	during boot up.
	Example: nofstab

	nolocale
	Options/Value: none, standalone flag
	The locale can be used to specify both language and country and 
	can be any combination of a language supported. 
	Example: nolocale
	
	nomodeset
	Options/Value: none, standalone flag
	Description: Specify that kernel don't initialize de X windows drivers.
	Example: nomodeset
	
	nonetworking
	Options/Value: none, standalone flag
	Description: Disable setup and configuration of networking during the boot sequence.
	Disables DHCP among other networking setup activities. This can be useful to speed up the
	boot sequence when networking is unimportant or established later.
	Example: nonetworking

	nopersistence
	Options/Value: none, standalone flag
	Description: Disable writing updated files to a storage device. This implies that 
	updated files are saved to RAM disk, via tmpfs. Any changes to those files will be lost on reboot.
	The persistence flag can be used to enable this option.
	Example: nopersistence

	noprompt
	Options/Value: none, standalone flag
	Description: Do not prompt to eject the CD or remove the USB flash drive on reboot.
	Example: noprompt
	
	noresume
	Options/Value: none, standalone flag
	Description: Disable resume from hibernation, even if the resume partition is given.
	Example: noresume

	nosplash
	Options/Value: none, standalone flag
	Description: Request that TAILS hide the distro splash screen during boot up.
	Example: nosplash

	noswap
	Options/Value: none, standalone flag
	Description: Disable swap at boot.
	Example: noswap
	
	panic=
	Options/Value: reboot-delay-seconds
	Description: When a fatal error occurs, force the initramfs scripts to reboot the system 
	after a delay specified in seconds. The default is to drop to a shell when a fatal error occurs.
	Example: panic=42

	persistence
	Options/Value: none, standalone flag
	Description: Enable writing updated files to a storage device. Many times a live distro will
	only save updated files to RAM disk, via tmpfs, and changes will be lost with reboots. 
	This flag will enable saving changes to a permanent storage device. The nopersistence flag can
	be used to disable this option.
	Example: persistence

	persistence-encryption=
	Options/Value: none, luks
	Description: A comma separated list of allowed encryption types for the persistence partition.
	If set to "luks", then the persistence filesystem must be of the type Linux Unified KeySetup.
	If the list includes "none", then an unencrypted persistence partition is allowed.
	Example: persistence-encryption=luks,none

	persistence-media
	Options/Value: removable, removable-usb
	Description: Limits the number of places to search for a persistence partition to be either
	removable media only or removable USB media only.
	Example: persistence-media=removable-usb

	persistence-method
	Options/Value: overlay
	Description: Comma separated list of supported persistence methods. As of 2013, only "overlay"
	seems to be supported and that setting is the default. If you specify this option/parameter 
	and fail to include "overlay", then unionfs overlay support for label specified, file-based 
	partition images will be disabled for the live boot.
	Example: persistence-method=overlay

	persistence-path
	Options/Value: Directory path within the boot partition.
	Description: An additional directory path used to search for and find file-based persistence 
	partition images. The root directory of the boot partition will also be searched for persistence
	partition images as well. You should only specify this option when you have created partition images
	for the purpose of supporting persistence.
	Example: persistence-path=persist

	persistence-read-only
	Options/Value: none, standalone flag
	Description: On the surface, this flag seems like an oxymoron. Normally, point of persistence
	is to save state between boots from the live media. This flag allows the use of unionfs and nfs 
	boot partitions with overlays but only mounting those overlays read-only. So, you can extend the 
	default filesystem with a custom overlay but block any updates to that partition image.
	Example: persistence-read-only

	persistence-storage
	Options/Value: filesystem,file
	Description: Controls what type of persistence partitions or file systems to search. The search 
	for a persistence partition is controlled by the label or name, see "persistence-label". 
	With "filesystem", the live boot script will attempt to locate a persistence partition with either 
	a matching partition name or containing a file system that has a matching file system label. 
	With "file", the live boot script will attempt to locate a persistence partition in a image 
	file that has a filename matching the given label. The default is "filesystem,file", which searches
	in both ways.
	Example: persistence-storage=file

	persistence-label
	Options/Value: Name or label of partition, filesystem or image.
	Description: Specifies a custom name or label used to identify the persistence partition or file system. 
	This file system is merged with the default root filesystem using unionfs to provide persistence between
	reboots of the live distro. You can specify a specific USB partition or file system image to use for
	persistence via this option. The live boot script will search various locations, including the
	boot file system root directory and boot device partitions. The default persistence label is
	"persistence". Keep in mind that the old, fixed persistence label names are still supported
	as well, namely "live-rw" and "home-rw".
	Example: persistence-label=mydata

	plainroot
	Options/Value: none, standalone flag
	Description: Instructs the boot script to ignore the standard device scan controlled by "live-media"
	and instead to mount the root file system from a standard block device. Use the "root=" boot parameter
	to specify which block device contains the root file system. The default is to search for the root file 
	system based on the "live-media" device setting.
	Example: plainroot

	quiet
	Options/Value: none, standalone flag
	Description: Suppress normal logging messages.
	Example: quiet

	quickusbmodules
	Options/Value: none, standalone flag
	Description: Disable loading extended USB support modules. If you use this flag, then only 
	USB device drivers compiled into the kernel are available. This flag can speed up the time 
	required to boot your live system. However, you may not have access to USB attached devices.
	Example: quickusbmodules

	ramdisk-size=
	Options/Value: size-in-bytes
	Description: The size of the RAM disk for the tmpfs writable area. This can be specified in bytes 
	or as a percentage with the "%" suffix. The default RAM disk size is half of physical memory.
	Example: ramdisk-size=65%

	resume=
	Options/Value: hibernation-partition
	Description: Specify a partition from which to resume a previous hibernation, if any.
	Example: resume=/dev/sda4
	Example: resume=UUID=c02dcbff-3222-4555-b333-c2351b73f88b
	Example: resume=LABEL=SWAPPART

	resume_offset=
	Options/Value: offset-within-partition
	Description: Specify the offset from which to begin the resume from hibernation.
	Example: resume_offset=0

	ro
	Options/Value: none, standalone flag
	Description: Mount the root filesystem read-only. Applies to live, local and nfs boots. 
	This is the opposite of the "rw" parameter/flag.
	Example: ro

	root=
	Options/Value: /dev/nfs, /dev/cifs
	Description: Place the initramfs script in NETBOOT mode, which causes it to download the root
	filesystem image from a specified server, as opposed to loading the image from local media.
	"root=/dev/nfs" is a synonym for "netboot=nfs". You will still need to specify "nfsroot=", "nfsopts=" 
	and "nfsoverlay=" as needed. "root=/dev/cif" is a synonym for "netboot=cifs". With the cifs option,
	you will also need to specify "nfsroot=" and "nfsopts=".
	Example: root=/dev/nfs

	root=LABEL=
	Options/Value: <partition-label>
	Description: Specify the label of the partition containing the root filesystem for the live system.
	The initramfs boot scripts will search the available and configured devices to find a matching partition
	and mount that partition, usually read-only, as the root filesystem.
	Example: root=LABEL=LIVEBOOT

	root=UUID=
	Options/Value: <fs-uuid>
	Description: Specify the UUID of the filesystem to mounted as the root filesystem for the live system.
	The initramfs boot scripts will search the available and configured devices to find a matching
	filesystem and mount it, usually read-only, as the root filesystem.
	Example: root=UUID=c02dcbff-3222-4555-b333-c2351b73f88b

	rootdelay=
	Options/Value: <device-timeout>
	Description: The length of time in seconds to wait for the root filesystem device to become available.
	Sometimes the initramfs boot script may be ready to access the device before the hardware has 
	completed its initialization. This timeout provides the maximum time to wait for the hardware.
	If and when the timeout expires, the boot script gives up and notifies the user of an error.
	The default for local block devices is 30 seconds. The default for encrypted local devices is 180 
	seconds. The default for nfs mounts is 180 seconds.
	Example: rootdelay=45

	rootflags=
	Options/Value: <mount-options>
	Description: Provide a comma separated list of extra mount options for the local root filesystem
	mount command. Do not specify "rootflags=ro" nor "rootflags=rw" as those options are already controlled
	by separate boot options, "ro" and "rw". The default is to provide no extra mount options for the root
	filesystem. Normally, there is no need to provide extra mount options.
	Example: rootflags=noatime
	Example: rootflags=data=journal

	rootfstype=
	Options/Value: <ext4, ext3, fat32, fat16, reiserfs>
	Description: Specify the filesystem type of the root filesystem. Normally it is not necessary to 
	specify the filesystem type because the initramfs scripts are quite good at properly detecting the
	correct type for an existing filesystem. However, you can use this parameter to specify the filesystem
	type explicitly to avoid any discrepancy.
	Example: rootfstype=ext4
	Example: rootfstype=fat32

	rw
	Options/Value: none, standalone flag
	Description: Mount the root filesystem for both read and write access. Applies to live, local and
	nfs boots. This is the opposite of the "ro" parameter/flag.
	Example: rw

	skipconfig
	Options/Value: none, standalone flag
	Description: Disable networking and the extra entries in fstab. If networking is unimportant and 
	there is not local DHCP server, this flag would probably speed up your boot time.
	Example: skipconfig

	skipunion
	Options/Value: none, standalone flag
	Description: Disable COW (copy on write) directory mounts in the union filesystem when using
	"exposedroot". Only applies if "exposedroot" is enabled. The default is to mount the COW directories
	anyway.
	Example: skipunion

	splash
	Options/Value: none, standalone flag
	Description: Request that TAILS display the distro splash screen during boot up.
	Example: splash

	todisk
	Options/Value: block device
	Description: Provides a writable block device to hold the live media or root filesystem. 
	This may boost performance of the live system through faster device access and in some cases
	by decompressing the live media during the copy step. Apparently, this option can be used on
	the GRUB or syslinux command line to copy the live media to another device during the boot sequence.
	For example, you might copy the live media when booting from a CDROM and specify a USB partition as
	the destination for subsequent boot from the properly configured USB drive.
	Example: todisk=/dev/sdb

	toram
	Options/Value: none, standalone flag
	Description: Request that TAILS copy the live media or root filesystem into RAM. This can be
	used to free the USB device for removal after the boot sequence finishes.
	Example: toram

	toram=
	Options/Value: filesystem image
	Description: Specify the filesystem image that should be copied to RAM for use as the live media
	or root file system of the live system. By specifying the filesystem image to copy to RAM, you 
	are also implicitly activating the run from RAM option, see the "toram" flag.
	Example: toram=livemedia.ext4

	ubi.mtd=
	Options/Value: mtd-partition-number[,nand-page-size]
	Description: Enable the ubi module/driver to work with a ubifs/ubi/mtd device/partition stack.
	This setup is specialized for operation on raw flash memory media, such as flash drives, that 
	need proper wear leveling. This driver stack is also optimized for the needs and restrictions of
	NAND based memory devices. According to Wikipedia, ubifs is the successor to JFFS2.
	Example: ubi.mtd=3 rootfstype=ubifs
	Example: ubi.mtd=1,2048 rootfstype=ubifs

	union=
	Options/Value: aufs, unionfs, overlayfs, unionfs-fuse, or unionmount
	Description: Specify the type of union filesystem to use when combining the multiple read-only and
	read-write partitions. The default union filesystem type is aufs.
	Example: union=unionfs
	
	vga=
	Options/Value: 769,771,773,775,784,787,790,793,785,788,791,794,834,884,786,789,792,795
	Description: Allows you to choose the VGA definition it uses when booting. 
	Example: vga=788 
	
Los parámetros citados anteriormente son los específicos para imágenes live. Hay muchas más opciones.
Podemos ver más aquí.

`http://www.mjmwired.net/kernel/Documentation/kernel-parameters.txt`
`http://manpages.ubuntu.com/manpages/oneiric/live-boot.7..html`

Llegados a esta parte, deberíamos ser capaces de administrar un servidor PXE avanzado.


### Asignar un menú según la procedencia del cliente

Realmente este es un paso sencillo, siempre que se conozca como 
funciona el arranque en pxe. Lo primero que deberíamos saber es que el 
servidor pxe nos otorgará un menú u otro en función del nombre que le 
otorguemos a nuestros archivos de configuración. En el caso que no haya
correspondéncia, se removerá un dígito hexadecimal y se volverá a comprobar.
En última instancia si no ha encontrado un archivo específico para el cliente
buscará el archivo por defecto llamado `default`.

Tenemos tres formas de darle identidad a nuestro archivo.

- Por UUID: Se usa el formato estándar de UUIDs, usando dígitos hexadecimales
en minúsculas. Por ejemplo b8945908-d6a6-41a9-611d-74a6ab80b83d

- Por MAC: Intercambiando los delimitadores por guiones y pasando a minúsculas.
Por ejemplo 88:99:AA:BB:CC:DD sería 01-88-99-aa-bb-cc-dd.

- Por IP: Pasando nuestra ip a hexadecimal y en mayúsculas 192.0.2.91 ->
C000025B. Tenemos una herramienta llamada `gethostip` que nos lo hace fácil.

Por si no ha quedado suficientemente claro, miremos el siguiente ejemplo.

Si el archivo de arranque es `/mybootdir/pxelinux.0`, el UUID is b8945908-d6a6-41a9-611d-74a6ab80b83d,
la MAC es 88:99:AA:BB:CC:DD y su IP es 192.0.2.91.

	/mybootdir/pxelinux.cfg/b8945908-d6a6-41a9-611d-74a6ab80b83d
	/mybootdir/pxelinux.cfg/01-88-99-aa-bb-cc-dd
	/mybootdir/pxelinux.cfg/C000025B
	/mybootdir/pxelinux.cfg/C000025
	/mybootdir/pxelinux.cfg/C00002
	/mybootdir/pxelinux.cfg/C0000
	/mybootdir/pxelinux.cfg/C000
	/mybootdir/pxelinux.cfg/C00
	/mybootdir/pxelinux.cfg/C0
	/mybootdir/pxelinux.cfg/C
	/mybootdir/pxelinux.cfg/default

	... en ese orden.


## Personalizando distros live

A todos nos ha pasado alguna vez, que disfrutamos mucho de una distribución,
pero nos faltan opciones y/o utilidades que no vienen instaladas por defecto.
En un sistema instalado no encontramos muchos problemas, ya que teniendo permisos
de superusuario podemos descargarnos los paquetes necesarios para implantar los 
programas deseados. En cambio en una distribución live es algo más engorroso, pero
para eso estamos aquí, para explicar paso a paso, como podemos hacerlo.

Lo primero que debemos tener en nuestras manos es la imagen live en formato `iso` 
que queremos modificar. 

La montaremos por ejemplo en el directorio /mnt para poder extraer el archivo que 
contiene el sistema de ficheros y que está comprimida en formato `squashfs`.

	$ sudo mount live.iso /mnt
	$ sudo cp /mnt/LiveOS/squashfs.img /var/tmp/

Una vez tenemos dicha imagen necesitamos descomprimirla en un 
directorio, que habremos creado previamente, para poder alterarla a 
nuestro antojo. Eso lo haremos gracias al paquete `squashfs-tools`.

	$ sudo yum install squashfs-tools
	$ mkdir /var/tmp/imagen
	$ unsquashfs -d /var/tmp/imagen/ -f /var/tmp/squashfs.img

En el directorio que hemos descomprimido la imagen `squashfs` encontraremos
una carpeta denominada `LiveOS` que sera la que contenga la imagen final del
sistema de archivos. (Normalmente su nombre será `ext3fs.img`). Esta imagen
la montaremos en un directorio para poder configurarla a nuestro parecer. 

	$ sudo mount /var/tmp/imagen/LiveOS/ext3fs.img /media

Llegados a este punto tenemos dos opciones.

- Editar los archivos a la &ldquo;brava&rdquo;.
- Crear una &ldquo;jaula&rdquo; y utilizar el directorio donde se ha montado
dicha imagen como sistema de archivos.

	**Aviso importante: Deberíamos cambiar la opción a disabled en el fichero
	`/etc/selinux/config` si no esta ya desactivado, ya que esta herramienta
	puede ser la causa de muchísimos errores a posteriori**

En la primera opción con unos conocimientos mínimos de sistemas tendremos 
suficiente. La segunda es un poco más rebuscada y por eso la explicaremos
paso a paso.

Crearemos un scprit, que será el encargado de montarnos una jaula.

Nos quedaría algo parecido a esto:

	#!/bin/bash
	LFS=/media

	# Preparacion de entorno
	mount -vt proc proc $LFS/proc
	mount -vt sysfs sysfs $LFS/sys
	mount -vt tmpfs tmpfs $LFS/run
	
	# Resolvedor de nombres
	cp /etc/resolv.conf $LFS/etc/
	
	# Pasamos el comando chroot, que es el encargado de cambiar 
	# la raíz del sistema de archivos.
	# Le indicamos también que nos reinicie las variables de entorno, 
	# le cambiamos el prompt, le definimos el PATH y el shell.
	chroot "$LFS" /usr/bin/env -i              \
		HOME=/root TERM="$TERM" PS1='\u:\w\$ ' \
		PATH=/bin:/usr/bin:/sbin:/usr/sbin     \
		/bin/bash --login

Al ejecutarlo, estaremos directamente dentro de la jaula listos para 
modificar al gusto del consumidor.

Cuando hayamos acabado de configurar nuestra futura distro &ldquo;tunning&rdquo;,
para salir de la jaula nos bastará pulsar conjuntamente `CTRL + D`. 

Una vez fuera solo tendremos que desmontar el directorio donde habíamos montado
el sistema de archivos y comprimir a squashfs el directorio imagen que teníamos
creado. (antes de este paso deberíamos mover la imagen squashfs antigua que 
teniamos por si las moscas).

	$ sudo umount /media/*
	$ sudo umount /media
	$ sudo mksquashfs /var/tmp/imagen /var/tmp/squashfs.img

Ahora nos queda insertar esta imagen o bien substituyendo la que ya 
tenga una iso o bien poniendola en el directorio, desde el que luego la 
llamaremos vía http por pxe, por ejemplo.

# Bibliografía

He intentado recolectar todas las páginas web que he ido utilizando para 
llegar a realizar este proyecto, siento si me he dejado alguna.

Como crear un livecd

https://fedoraproject.org/wiki/How_to_create_and_use_a_Live_CD

Isos varias

http://isoredirect.centos.org/centos/7/isos/x86_64/CentOS-7-x86_64-Everything-1503-01.iso

https://fedoraproject.org/wiki/FedoraLiveCD/es

Explicaciones pxe

http://wiki.centos.org/es/HowTos/PXE/PXE_Setup

http://docs.fedoraproject.org/en-US/Fedora/17/html/Installation_Guide/s1-netboot-pxe-config.html

http://docs.fedoraproject.org/en-US/Fedora/21/html/Installation_Guide/pxe-tftpd.html

http://www.syslinux.org/old/pxe.php 

Dhcp server

http://docs.fedoraproject.org/en-US/Fedora/15/html/Deployment_Guide/s1-dhcp-configuring-server.html

Distribución linux mini

http://www.tinycorelinux.net/downloads.html 

Distribución Gparted

http://gparted.org/livepxe.php 
 
Menús pxe

http://wiki.centos.org/es/HowTos/PXE/PXE_Setup/Menus 

http://www.syslinux.org/wiki/index.php/PXELINUX 

https://craftedflash.com/info/live-distro-boot-parameters 

Modificación de imágenes ya creadas

http://gnulinuxvagos.es/topic/21-crearmodificar-un-livecd-de-debian-y-no-morir-en-el-intento/ 

http://es.wikibooks.org/wiki/Personalizar_distribuci%C3%B3n_de_Ubuntu_Live_CD 

Markdown & Gitlab

http://markable.in/editor/ 

http://porteus-kiosk.org/download.html 

https://gitlab.com 

http://plugins.geany.org/markdown.html 

Guión y diario cronológico en Google docs

https://docs.google.com/document/d/1fBcLRBZEXDOJ1mIDzl8Us3TdSvUufeS7OJnf5Uh4CPc/

Documentación e isos varias

En los repositorios de la escola del treball.

# Agradecimientos

Primero agradecer a todos los que hayáis leído hasta aquí. Intentaré hacer mención
a todas las personas que creo que han hecho posible este proyecto, aunque me adelanto
a pedir disculpas si me olvido de alguién.


Gracias..
  
  - al profesorado en general de la escola del treball, en especial a Eduard Canet y
  Joan Ordinas por resolver y ayudar en momentos de colapso.
  
  - a mi compañero de clase Eugeni Lvov por ayudarme a enjaular un sistema de archivos.

  - a mi compañero de clase y amigo Ignacio Cegarra por resolver muchas de las dudas 
  que siempre me resuelve.

  - a mi compañero de clase Guillem Escuder por sus maravillosos apuntes y su ayuda con `markdown`.
  
  - a mi compañero de clase y proyecto Héctor Arnese por compartir dudas y soluciones.



