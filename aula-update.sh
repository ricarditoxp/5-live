#/bin/bash
echo "---------------------------------------------------"
echo "POST Actualització paquets de Fedora20 aula alumnes"
echo "desembre 2014 @edt"
echo "---------------------------------------------------"

# Cal desactivar selinux
    echo "desactiva selinux!!!"
    #sed -i 's/SELINUX=enforcing/SELINUX=disabled/' /etc/selinux/config
    #cp /etc/selinux/config /etc/selinux/config$$
    sed -i 's/SELINUX=enforcing/SELINUX=disabled/' /etc/selinux/config

# Configurem serveis apropiadament:
    systemctl enable NetworkManager-wait-online.service
    systemctl disable firewalld.service
    systemctl stop firewalld.service

# gpm:
    yum -y install gpm vim vim-minimal
    systemctl enable gpm.service
    systemctl start gpm.service

# Configuració amb el kerberos:
    yum -y install  krb5-devel krb5-workstation nfs-utils pam_mount
    #authconfig-tui
    authconfig --enableshadow --enablelocauthorize --enableldap --ldapserver='ldap' --ldapbase='dc=escoladeltreball,dc=org' --enablekrb5 --krb5kdc='gandhi' --krb5adminserver='gandhi' --krb5realm='INFORMATICA.ESCOLADELTREBALL.ORG' --updateall
    cd /tmp
    wget ftp://gandhi/pub/kerberos-nfs-f20.tgz
    tar -C / -xvzf kerberos-nfs-f20.tgz
    systemctl enable nfs-secure.service
    systemctl restart nfs-secure.service
    dconf update

# Montar el sistema de fitxers 
    echo "gandhi:/groups /home/groups nfs4 sec=krb5,comment=systemd.automount 0 0" >> /etc/fstab

# Excloure les actualitzacions del kernel
    echo "exclude=kernel*" >> /etc/yum.conf

# Configurar l'arrancada al mode multi-user.target
    #rm -f /etc/systemd/system/default.target
    #ln -s /lib/systemd/system/multi-user.target /etc/systemd/system/default.target

# repositoris:
    rpm -ivh http://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-stable.noarch.rpm
    rpm -ivh http://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-stable.noarch.rpm
    rpm -ivh http://linuxdownload.adobe.com/adobe-release/adobe-release-x86_64-1.0-1.noarch.rpm
    rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-adobe-linux
    yum -y install flash-plugin

# Actualitzar el sistema
    #yum -y update
# Altres
    yum -y install vlc nmap wireshark-gnome  ethtool @Virtualization

# Instal·ació de virt-manager i permetre'n l'ús pels usuaris normals
    yum -y install virt-manager libvirt-daemon-driver libvirt-daemon-config-network
    sed -i 's/#auth_unix_rw = "none"/auth_unix_rw = "none"/' /etc/libvirt/libvirtd.conf
    systemctl restart libvirtd.service
#
exit
