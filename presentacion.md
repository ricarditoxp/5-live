% Imágenes live personalizadas arrancadas por PXE.
% Ricardo Marcelo Rando
% isx46360391

# ¿Qué es una distribución de tipo Live?

- **Introducción**

- **Explicación**

- **Syslinux, el proyecto Linux**

# Tipos de distribuciones Live

- **Desktop** 

- **Instaladores**

- **Rescue**

- **Tools**

- **Kiosko**

# Como crear tu propia distribución

- **Livecd-tools:** livecd-creator,livecd-iso-to-disk, livecd-to-pxe.. 

- **Kikcstarts:** Archivos .ks, system-config-kickstart, spin-kickstarts..


# Soportes para distribuciones live

- **CD o DVD:** Brasero.

- **Dispositivo USB:** liveusb-creator.

- **PXE**

# PXE a fondo

- **HTTP**

- **TFTP**

- **DHCP**

# Configurando PXE

- **Elementos necesarios:** Kernel, initrd, squashf..

- **Estructura**

##

	tftpboot						/var/www/html
	├── live							└──── live
	│   ├── initrd.img					   └── squashfs.img
	│   └── vmlinuz					
	├── pxelinux.0					
	├── pxelinux.cfg				
	│   ├── default					
	└── vesamenu.c32				

# Configurando PXE

- **Imagenes y menús PXE**

##

	#archivo del menú principal
	DEFAULT vesamenu.c32
	PROMPT 0
	MENU BACKGROUND images/index.jpg
	MENU TITLE PXE MENU

	LABEL ^1 Instaladores
			KERNEL vesamenu.c32
			APPEND pxelinux.cfg/instaladores
			TEXT HELP
			Instaladores de sistemas operativos
			ENDTEXT
	LABEL ^2 Versiones Live
			KERNEL vesamenu.c32
			APPEND pxelinux.cfg/live
			TEXT HELP
			Versiones de tipo Live
			ENDTEXT

# Configurando PXE

	#submenú de distribuciones live
	DEFAULT vesamenu.c32
	PROMPT 0
	TIMEOUT 0

	MENU BACKGROUND images/splash.jpg
	MENU TITLE Live

	LABEL ^1 Centos Live 7 Desktop
			KERNEL centos7/vmlinuz
			APPEND initrd=centos7/initrd.img root=live:http://192.168.0.10/centos7live/squashfs.img
			TEXT HELP
			Live de centos 7 desktop
			ENDTEXT 
	LABEL ^2 Tiny Core Live
			KERNEL tiny/vmlinuz
			APPEND initrd=tiny/initrd.img boot=live
			TEXT HELP
			Mini Linux Live
			ENDTEXT
	LABEL ^3 Fedora Live KDE 21
			KERNEL fedora21/vmlinuz
			APPEND initrd=fedora21/initrd.img root=live:http://192.168.0.10/fedora-live-kde-21/squashfs.img



# Personalizando distros live

- **Asignar un menú según la procedencia del cliente**

## 

	Si el archivo de arranque es `/mybootdir/pxelinux.0`, el UUID is b8945908-d6a6-41a9-611d-74a6ab80b83d,
	la MAC es 88:99:AA:BB:CC:DD y su IP es 192.0.2.91.

	/mybootdir/pxelinux.cfg/b8945908-d6a6-41a9-611d-74a6ab80b83d
	/mybootdir/pxelinux.cfg/01-88-99-aa-bb-cc-dd
	/mybootdir/pxelinux.cfg/C000025B
	/mybootdir/pxelinux.cfg/C000025
	/mybootdir/pxelinux.cfg/C00002
	/mybootdir/pxelinux.cfg/C0000
	/mybootdir/pxelinux.cfg/C000
	/mybootdir/pxelinux.cfg/C00
	/mybootdir/pxelinux.cfg/C0
	/mybootdir/pxelinux.cfg/C
	/mybootdir/pxelinux.cfg/default

	... en ese orden.

# Personalizando distros live

- **Como personalizar una distro ya existente:** squashfs-tools,chroot..

##

	mksquashfs vs unsquashfs

##

	# script para enjaular un directorio, en este caso /media
	#!/bin/bash
	LFS=/media

	# Preparacion de entorno
	mount -vt proc proc $LFS/proc
	mount -vt sysfs sysfs $LFS/sys
	mount -vt tmpfs tmpfs $LFS/run
	
	# Resolvedor de nombres
	cp /etc/resolv.conf $LFS/etc/
	
	# Pasamos el comando chroot, que es el encargado de cambiar 
	# la raíz del sistema de archivos.
	# Le indicamos también que nos reinicie las variables de entorno, 
	# le cambiamos el prompt, le definimos el PATH y el shell.
	chroot "$LFS" /usr/bin/env -i              \
		HOME=/root TERM="$TERM" PS1='\u:\w\$ ' \
		PATH=/bin:/usr/bin:/sbin:/usr/sbin     \
		/bin/bash --login

# Personalizando distros live

- **Personalizaciones propias:** kiosko personalizado, centos aula

# Bibliografía

- **De dónde sale toda esta información**

# Agradecimientos

- **¡Gracias!**
